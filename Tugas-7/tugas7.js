//soal 1
class Car {
  constructor(brand) {
    this.carname = brand;
  }
  get cnam() {
    return this.carname;
  }
  set cnam(x) {
    this.carname = x;
  }
}

mycar = new Car("Ford");
console.log(mycar.cnam); // Ford
// getter cnam digunakan tanpa "()"

 class Car2 {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return 'I have a ' + this.carname;
  }
}

class Model extends Car2 {
  constructor(brand, mod) {
    super(brand);
    this.model = mod;
  }
  show() {
    return this.present() + ', it is a ' + this.model;
  }
}

mycar = new Model("Ford", "Mustang");
console.log(mycar.show());

class Animal{
	constructor(name){
		this.name = name;
		this.legs = 4;
		this.cold_blooded = false;
	}

	get AniName(){
		return this.name;
	}
	set AniName(x){
		this.name = x;
		
	}
}
	var sheep = new Animal("shaun");
 
console.log(sheep.AniName) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Frog extends Animal{
	constructor(name){
		super(name);
		
	}
	jump(){
		return "hop-hop";
	}
}
var kodok = new Frog("buduk")
console.log(kodok.jump())

class Ape extends Animal{
	constructor(name){
		super(name);
		
	}
	yell(){
		return "Auooo";
	}
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"



//soal 2
function Clock({ template }) {
  
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 

class Clock2 {
    constructor(template){
    	this.template = template;
    }
    get render(template){
   var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output); 	
    }
 	stop(timer) {
    	clearInterval(timer);
  	};

  	start(timer) {
    	render();
    	timer = setInterval(render, 1000);
  	};
}

var clock = new Clock2({template: 'h:m:s'});
clock.start();  