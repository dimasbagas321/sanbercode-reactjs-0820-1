//soal 1
var flag = 2
console.log("LOOPING PERTAMA")
while(flag <= 20){
	console.log(flag + " - I Love Coding");
	flag++;
}
var flag2 = 20
console.log("LOOPING KEDUA")
while(flag2 >= 2){
	console.log(flag2 + " - I will become a frontend developer")
	flag2--;
}

//soal 2
for(var deret = 1; deret <= 20; deret++){
	if(deret%3 == 0 && deret%2 == 1){
		console.log(deret + " - I Love Coding");
	}else if(deret%2 == 0){
		console.log(deret + ' - Berkualitas');
	}else if(deret%2 == 1){
		console.log(deret + ' - Santai')
	}
}

//soal 3
var row = 7;

for (var i = 1; i <= row; i++) {
	var result = '';
	for (var j = 1; j <= i; j++) {
		result += "#";	
	}
	console.log(result);	
}

//soal 4
var kalimat="saya sangat senang belajar javascript"
 var putus = kalimat.split(" ");
 console.log(putus);


//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
console.log(daftarBuah[0]);
console.log(daftarBuah[1]);
console.log(daftarBuah[2]);
console.log(daftarBuah[3]);
console.log(daftarBuah[4]);
