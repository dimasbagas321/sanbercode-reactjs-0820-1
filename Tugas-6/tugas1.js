//soal 1
let phi = 22/7;
let r = 7;

const luasLingkaran = () => {
 	return phi*(r*r);
}

const kelilingLingkaran = () => {
	return 2*phi*r;
}
console.log(luasLingkaran());
console.log(kelilingLingkaran());

//soal 2
const text1 = "Saya";
const text2 = "adalah";
const text3 = "Seorang";
const text4 = "Frontend";
const text5 = "Developer";

let kalimat = `${text1} ${text2} ${text3} ${text4} ${text5}`;
console.log(kalimat);

//soal 3
const newFunction = (firstName, lastName) =>{
    return{firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName)
     return 
    }
}
} 
//Driver Code 
newFunction("Dimas Bagas", "Prakoso").fullName();

//soal 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

//soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

