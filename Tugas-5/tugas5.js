//soal 1
function halo(){
	var halo = "Halo Sanbers!";
	return halo;
}

console.log(halo());

//soal 2
function kalikan(num1, num2){
	return num1*num2;
}
var num1 = 12;
var num2 = 4;

var  hasilKali = kalikan(num1, num2);
console.log(hasilKali);

//soal 3
function introduce(nama, umur, alamat, hobi){
	return "Nama saya " + nama + ", Umur saya " + umur + " tahun, alamat saya di " + alamat + ", hobby saya yaitu " + hobi;
}

var name = " Bagas";
var age = 19;
var address = "Hilir";
var hobby = "Mancing";
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

//soal 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var ObjectDaftarPeserta = {
	nama : "Asep",
	"jenis kelamin" : "laki-laki",
	hobi : "baca buku",
	"tahun lahir" : 1992,
}
// console.log(arrayDaftarPeserta);
console.log(ObjectDaftarPeserta);

//soal 5
	var buah = [
					{nama : "Strawberry", warna : "Merah", "ada biji" : "tidak", harga : 9000},
					{nama : "Jeruk", warna : "Oranye", "ada" : "tidak", harga : 8000},
					{nama : "Semangka", warna : "Hiju & Merah", "ada biji" : "ada", harga : 10000},
					{nama : "Pisang", warna : "Kuning", "ada biji" : "tidak", harga : 5000},
				]
	console.log(buah[0]);

//soal 6
var dataFilm = [];
dataFilm.push({nama : "Pes en Porius", durasi : "1 jam" , genre : "Action", tahun : "2017"});
dataFilm.push({nama : "Dora-chan", durasi : "2 jam" , genre : "Adventure", tahun : "2019"});
dataFilm.push({nama : "Tois Setori", durasi : "1.5 jam" , genre : "Comedy", tahun : "2019"});
console.debug(dataFilm[0s]);
