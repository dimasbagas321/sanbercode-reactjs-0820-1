//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kt1Wp = kataPertama + ' '
var kt2Up = kataKedua.charAt(0).toUpperCase()
var kt2Combine = kt2Up.concat(kataKedua.substr(1,5)) + ' '
var kt3Wp = kataKetiga + ' '
var kt4Up = kataKeempat.toUpperCase()
var ktCombineFull = kt1Wp.concat(kt2Combine.concat(kt3Wp.concat(kt4Up)))
console.log(ktCombineFull)

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var kt1ToInt = parseInt(kataPertama)
var kt2ToInt = parseInt(kataKedua)
var kt3ToInt = parseInt(kataKetiga)
var kt4ToInt = parseInt(kataKeempat)
var intJumlah = kt1ToInt+kt2ToInt+kt3ToInt+kt4ToInt
console.log(intJumlah)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 100;
if(nilai >= 80 && nilai <= 100){
	console.log("Nilai anda : " + nilai + " dengan predikat A")
}else if(nilai >=70 && nilai < 80){
	console.log("Nilai anda : " + nilai + " dengan predikat B")
}else if(nilai >= 60 && nilai < 70){
	console.log("Nilai anda : " + nilai + " dengan predikat C")
}else if (nilai >=50 && nilai < 60){
	console.log("Nilai anda : " + nilai + " dengan predikat D")
}else if(nilai < 50 && nilai > 0){
	console.log("Nilai anda : " + nilai + " dengan predikat E")
}else{
	console.log("anda belum dapat nilai")
}

//soal 5
var tanggal = 29;
var bulan = 11;
var tahun = 2000;

switch(bulan){
	case 1: {bulan = "Januari"; break;}
	case 2: {bulan = "Februari"; break;}
	case 3: {bulan = "Maret"; break;}
	case 4: {bulan = "April"; break;}
	case 5: {bulan = "Mei"; break;}
	case 6: {bulan = "Juni"; break;}
	case 7: {bulan = "Juli"; break;}
	case 8: {bulan = "Agustus"; break;}
	case 9: {bulan = "September"; break;}
	case 10: {bulan = "Oktober"; break;}
	case 11: {bulan = "November"; break;}
	case 12: {bulan = "Desember"; break;}
}
var ttl = tanggal + " " + bulan + " " + tahun;
console.log(ttl);