var sayHello = "Hello World!"
var andBye = " & Good Bye!"
console.log(sayHello)
console.log(sayHello[6])
console.log(sayHello.length)
console.log(sayHello.charAt(3).toUpperCase())
console.log(sayHello.concat(andBye))
console.log(andBye.indexOf("& Goo"))
console.log(andBye.substr(4))
console.log(andBye.substr(4, 2))
console.log(sayHello.toLowerCase())
console.log(andBye.toUpperCase())
console.log(andBye.trim())
